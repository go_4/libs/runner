package runner

import (
	"math/rand"
	"time"
)

type Action string

type Route string

const (
	requestIDLength = 30
)

const (
	GithubRunner Route = "/github"
	Jobs         Route = "/jobs"
	Status       Route = "/status"
	Webhook      Route = "/webhook"
)

func GenerateRequestID() string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

	rand.Seed(time.Now().UnixNano())

	b := make([]rune, requestIDLength)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
