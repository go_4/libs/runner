package github

import "gitlab.com/go_4/libs/runner/runner"

const (
	RegisterRunner    runner.Action = "registerRunner"
	UnregisterRunner  runner.Action = "unregisterRunner"
	ListAllRunners    runner.Action = "listAllRunners"
	ListLabelsRunners runner.Action = "listLabelsRunners"
	RemoveRunner      runner.Action = "removeRunner"
	DeleteRunner      runner.Action = "deleteRunner"
)

type GithubEvent struct {
	RequestID    string        `json:"requestId"`
	Action       runner.Action `json:"action"`
	Repository   *Repository   `json:"repository"`
	Token        *Token        `json:"token,omitempty"`
	RunnerID     *int          `json:"runnerID,omitempty"`
	Runners      []Runners     `json:"runners,omitempty"`
	FiltersLabel []string      `json:"filtersLabel,omitempty"`
}

type Repository struct {
	Owner string `json:"owner"`
	Name  string `json:"name"`
}

type Token struct {
	Token     string `json:"token"`
	ExpiresAt string `json:"expires_at"`
}

type Runners struct {
	ID     int      `json:"id"`
	Name   string   `json:"name"`
	Os     string   `json:"os"`
	Status string   `json:"status"`
	Busy   bool     `json:"busy"`
	Labels []Labels `json:"labels"`
}

type Labels struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}
