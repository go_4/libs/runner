package clean

import "gitlab.com/go_4/libs/runner/runner"

type EC2StatusCode int
type RunnerStatus string

const (
	CleanInstances runner.Action = "cleanInstances"
	CleanOffline   runner.Action = "cleanOffline"

	EC2Pending      EC2StatusCode = 0
	EC2Running      EC2StatusCode = 16
	EC2ShuttingDown EC2StatusCode = 32
	EC2Terminated   EC2StatusCode = 48
	EC2Stopping     EC2StatusCode = 64
	EC2Stopped      EC2StatusCode = 80

	Offline      RunnerStatus = "Offline"
	SelfRemoved  RunnerStatus = "SelfRemoved"
	ForceRemoved RunnerStatus = "ForceRemoved"
)

type CleanEvent struct {
	RequestID string        `json:"requestId"`
	Action    runner.Action `json:"action"`
}

type RunnerItem struct {
	RunnerID            string `dynamodbav:"RunnerID,omitempty" json:"runnerID"`
	TimeToLive          int64  `dynamodbav:"TimeToLive,omitempty" json:"timeToLive"`
	OfflineDetectedTime string `dynamodbav:"OfflineDetectedTime,omitempty" json:"offlineDetectedTime"`
	ProjectName         string `dynamodbav:"ProjectName,omitempty" json:"projectName"`
	Status              string `dynamodbav:"Status,omitempty" json:"status"`
}
