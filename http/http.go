package http

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	"go.uber.org/zap"
)

type HTTPRequest struct {
	Method    string             `json:"method"`
	ApiURL    string             `json:"apiUrl"`
	Headers   map[string]string  `json:"headers"`
	Body      []byte             `json:"body"`
	RequestID string             `json:"requestId"`
	Logger    *zap.SugaredLogger `json:"logger"`
}

var (
	errorHTTPRequest      = errors.New("There was problem to make http request")
	errorCloseHTTPRequest = errors.New("There was problem to close http request")
	errorReadHTTPBody     = errors.New("There was problem to read http body")
)

func GetHeadersPlainText(requestID string) map[string]string {
	return map[string]string{
		"ISLE-Request-ID": requestID,
		"Content-Type":    "text/plain; charset=utf-8",
	}
}

func GetHeadersJSON(requestID string) map[string]string {
	return map[string]string{
		"ISLE-Request-ID": requestID,
		"Content-Type":    "application/json",
	}
}

func GetHeadersGithub(token string, requestID string) map[string]string {
	return map[string]string{
		"ISLE-Request-ID": requestID,
		"Accept":          "application/vnd.github+json",
		"Authorization":   fmt.Sprintf("token %s", token),
	}
}

func GetLambdaAuthHeaders(token string, requestID string) map[string]string {
	return map[string]string{
		"ISLE-Request-ID": requestID,
		"Content-Type":    "application/json",
		"Authorization":   token,
		"ISLE-Marker":     "isle",
	}
}

func HTTPQuery(ctx context.Context, request HTTPRequest) (*http.Response, []byte, error) {

	requestBody := &bytes.Buffer{}
	if request.Body != nil {
		requestBody = bytes.NewBuffer(request.Body)
	}

	req, err := http.NewRequest(request.Method, request.ApiURL, requestBody)
	if err != nil {
		request.Logger.Errorw(
			errorHTTPRequest.Error(),
			"RequestID", request.RequestID,
			"Error", err.Error(),
		)
		return nil, nil, err
	}

	for k, v := range request.Headers {
		req.Header.Add(k, v)
	}

	req = req.WithContext(ctx)

	client := &http.Client{
		Timeout: time.Second * 10,
	}

	resp, err := client.Do(req)
	if err != nil {
		request.Logger.Errorw(
			errorCloseHTTPRequest.Error(),
			"RequestID", request.RequestID,
			"Error", err.Error(),
		)
		return nil, nil, err
	}

	defer resp.Body.Close() //after close body we cannot access it any more, so we need extract it now

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		request.Logger.Errorw(
			errorReadHTTPBody.Error(),
			"RequestID", request.RequestID,
			"Error", err.Error(),
		)
		return nil, nil, err
	}

	return resp, body, nil
}
