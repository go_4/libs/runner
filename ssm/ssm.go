package ssm

type SSM struct {
	Name             string   `json:"name"`
	Labels           []string `json:"labels"`
	LaunchTemplateID string   `json:"launch_template_id"`
}
