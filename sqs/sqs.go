package sqs

import "gitlab.com/go_4/libs/runner/runner"

type SQSEvent struct {
	Action    runner.Action `json:"action"`
	RequestID string        `json:"requestId"`
	Message   *SQS          `json:"message"`
}

type SQS struct {
	EventType       string   `json:"eventType"`
	ID              *int     `json:"id"`
	InstallationID  *int     `json:"installationId"`
	Labels          []string `json:"labels"`
	RepositoryName  string   `json:"repositoryName"`
	RepositoryOwner string   `json:"repositoryOwner"`
	RequestID       string   `json:"requestId"`
	RunID           *int     `json:"runID"`
}
