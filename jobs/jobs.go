package jobs

import "gitlab.com/go_4/libs/runner/runner"

const (
	List   runner.Action = "list"
	Get    runner.Action = "get"
	Put    runner.Action = "put"
	Delete runner.Action = "delete"
	Update runner.Action = "update"
)

type JobEvent struct {
	RequestID  string        `json:"requestId"`
	Action     runner.Action `json:"action"`
	WorkflowID int           `json:"workflowId"`
	Item       *JobItem      `json:"item,omitempty"`
	Items      []JobItem     `json:"items,omitempty"`
}

type JobItem struct {
	WorkflowID int    `dynamodbav:"WorkflowID,omitempty" json:"workflowId"`
	RequestID  string `dynamodbav:"RequestID,omitempty" json:"requestId"`
	TimeToLive int64  `dynamodbav:"TimeToLive,omitempty" json:"timeToLive"`
	CreateDate string `dynamodbav:"CreateDate,omitempty" json:"createDate"`
	RunID      int    `dynamodbav:"RunID,omitempty" json:"runID"`
}
