package status

import "gitlab.com/go_4/libs/runner/runner"

type RunnerStatus string

const (
	List   runner.Action = "list"
	Get    runner.Action = "get"
	Put    runner.Action = "put"
	Delete runner.Action = "delete"
	Update runner.Action = "update"

	Created             RunnerStatus = "Created"
	Configured          RunnerStatus = "Configured"
	RunnerStarted       RunnerStatus = "RunnerStarted"
	FinishedJob         RunnerStatus = "FinishedJob"
	Terminated          RunnerStatus = "Terminated"
	CleanedRunnerConfig RunnerStatus = "CleanedRunnerConfig"
	GithubManualRemoved RunnerStatus = "GithubManualRemoved"
)

type StatusEvent struct {
	RequestID  string        `json:"requestId"`
	Action     runner.Action `json:"action"`
	InstanceID string        `json:"instanceId"`
	Item       *StatusItem   `json:"item,omitempty"`
	Items      []StatusItem  `json:"items,omitempty"`
}

type StatusItem struct {
	InstanceID              string `dynamodbav:"InstanceID,omitempty" json:"instanceId"`
	RequestID               string `dynamodbav:"RequestID,omitempty" json:"requestId"`
	LastStatus              string `dynamodbav:"LastStatus,omitempty" json:"lastStatus"`
	CreateDate              string `dynamodbav:"CreateDate,omitempty" json:"createDate"`
	ConfigDate              string `dynamodbav:"ConfigDate,omitempty" json:"configDate"`
	RunningDate             string `dynamodbav:"RunningDate,omitempty" json:"runningDate"`
	FinishJobDate           string `dynamodbav:"FinishJobDate,omitempty" json:"finishJobDate"`
	TerminateDate           string `dynamodbav:"TerminateDate,omitempty" json:"terminateDate"`
	GithubManualRemovedDate string `dynamodbav:"GithubManualRemovedDate,omitempty" json:"githubManualRemovedDate"`
	TimeToLive              int64  `dynamodbav:"TimeToLive,omitempty" json:"timeToLive"`
	// InstanceID    string `dynamodbav:"InstanceID,omitempty" json:"instanceId,omitempty"`
	// LastStatus    string `dynamodbav:"LastStatus,omitempty" json:"lastStatus,omitempty"`
	// CreateDate    string `dynamodbav:"CreateDate,omitempty" json:"createDate,omitempty"`
	// ConfigDate    string `dynamodbav:"ConfigDate,omitempty" json:"configDate,omitempty"`
	// RunningDate   string `dynamodbav:"RunningDate,omitempty" json:"runningDate,omitempty"`
	// FinishJobDate string `dynamodbav:"FinishJobDate,omitempty" json:"finishJobDate,omitempty"`
	// TerminateDate string `dynamodbav:"TerminateDate,omitempty" json:"terminateDate,omitempty"`
}
